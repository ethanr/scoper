#!/usr/bin/env python
from __future__ import print_function, with_statement
import sys
import codecs
import os
import chardet      # 3rd party library
import docopt       # 3rd party library
from parsers import ipv4, ipv6, hostnames, emails
from operations import *

__author__ = 'Ethan Robish'

PARSERS = ('ipv4', 'ipv6', 'hostnames', 'emails')
OPERATIONS = ('union', 'intersection', 'difference', 'cidr', 'expand', 'equal', 'size', 'resolve')

USAGE = """\
Usage:
  {prog} [--ipv4 | --ipv6 | --hostnames | --emails] <command>
  {prog} -h | --help

Besides typical Python syntax, these functions are also available to use: {operations}
union, intersection, and difference can each take any number of arguments. 
difference works by using the first argument as the starting set and taking every other argument out of that set.
If you do not specify a parser to use on the command line, you will need to call these manually in <command>: {parsers}
  
Options:
  -h --help     Show this screen.
  --ipv4        Parse all arguments within the command for IPv4 addresses.
  --ipv6        Parse all arguments within the command for IPv6 addresses.
  --hostnames   Parse all arguments within the command for hostnames.
  --emails      Parse all arguments within the command for emails.
  
Arguments:
  <command>     Python command to execute, surrounded by quotes.
  
Examples:
  {prog} --ipv4 "equal('192.168.1.1', '192.168.1.2')" => False
  {prog} --ipv4 "union('192.168.1.1', '192.168.1.2')" => 192.168.1.1 192.168.1.2
  {prog} --ipv4 "cidr(intersection('10.0.0.0/8', '10.10.10.10/24'))" => 10.10.10.0/24
  {prog} "intersection(resolve('google.com'), ipv4('208.53.243.80'))" => 208.53.243.80
""".format(prog=sys.argv[0], operations=', '.join(OPERATIONS), parsers=', '.join(PARSERS))

def get_parser(args):
    parser = None
    if args['--ipv4']:
        parser = ipv4
    if args['--ipv6']:
        parser = ipv6
    if args['--hostnames']:
        parser = hostnames
    if args['--emails']:
        parser = emails
    return parser

# Arguments must be processed before the normalize_input function is defined.
ARGS = docopt.docopt(USAGE)
PARSER = get_parser(ARGS)

def read_file(filename):
    # handle files with other encodings like UTF-16
    # source: http://stackoverflow.com/a/13591421
    bytes = min(32, os.path.getsize(filename))
    raw = open(filename, 'rb').read(bytes)

    if raw.startswith(codecs.BOM_UTF8):
        encoding = 'utf-8-sig'
    else:
        result = chardet.detect(raw)
        encoding = result['encoding']

    with codecs.open(filename, encoding=encoding) as inf:
        return inf.read()

def normalize_input(func):
    def new_func(*args):
        '''Normalize args by converting any files or strings to sets.'''
        text = None
        # create a copy of args
        new_args = list(args)
        for i, arg in enumerate(args):
            # check if arg is a string
            if isinstance(arg, str):
                # parse the file if the file exists
                text = read_file(arg) if os.path.isfile(arg) else arg
                # parse the string to get a set or set-like object
                new_args[i] = PARSER(text) if PARSER is not None else text
        # finally, call the original function with the new arguments
        return func(*new_args)
    return new_func

if __name__ == '__main__':
    command = ARGS['<command>']
    result = eval(command)
    if hasattr(result, '__iter__'):
        print('\n'.join(map(str, result)))
    else:
        print(result)