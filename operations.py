import operator
import socket
import netaddr      # 3rd party library
from scoper import normalize_input

__author__ = 'Ethan Robish'

# Change the behavior of normalize_input whether this is ran from command line or used as an import libray.
# if __name__ == '__main__':
#     from scoper import normalize_input
# else:
#     def normalize_input(func):
#         def new_func(*args):
#             '''Normalize args by converting all arguments to sets.'''
#             # create a copy of args
#             new_args = list(args)
#             for i, arg in enumerate(args):
#                 # check if arg is a set or a subclass of a set already
#                 if not isinstance(arg, set) and not issubclass(set, arg.__class__):
#                     # turn arg into a set
#                     new_args[i] = set(arg)
#             # finally, call the original function with the new arguments
#             return func(*new_args)
#         return new_func

@normalize_input
def union(*args):
    return sorted(reduce(operator.__or__, args))

@normalize_input
def intersection(*args):
    return sorted(reduce(operator.__and__, args))

@normalize_input
def difference(*args):
    return sorted(reduce(operator.__sub__, args))

@normalize_input
def equal(*args):
    # check all arguments against its neighbor
    return all(map(operator.__eq__, args, args[1:] + args[:1]))

@normalize_input
def size(data):
    return len(data)

@normalize_input
def cidr(ip_set):
    """Returns a list"""
    return netaddr.cidr_merge(ip_set)

@normalize_input
def expand(cidr_list):
    """Returns a netaddr.IPSet"""
    return netaddr.IPSet(cidr_list)

def _resolve_hostname(d):
    """
    This method returns an array containing
    one or more IP address strings that respond
    as the given domain name

    source: http://stackoverflow.com/questions/3837744/how-to-resolve-dns-in-python
    """
    ''
    try:
        # TODO: Ensure all domain names are returned
        data = socket.gethostbyname_ex(d)
        return data[2]
    except Exception:
        # fail gracefully!
        return []
        
def resolve(*args):
    """Returns a netaddr.IPSet"""
    ip_set = netaddr.IPSet()
    
    for hostname in args:
        ip_set |= netaddr.IPSet(_resolve_hostname(hostname))

    return ip_set