import re
import netaddr      # 3rd party library

__author__ = 'Ethan Robish'

def ipv4(text):
    """Returns a netaddr.IPSet"""
    
    # source: StackOverflow
    ipv4_cidr_regex = re.compile(r"""(
        (
            (
                [0-9]
                |[1-9][0-9]
                |1[0-9]{2}
                |2[0-4][0-9]
                |25[0-5]
            )                   # match any number 0 up to 255
            \.                  # followed by a full-stop
        ){3}                    # repeated three times (the first three parts of an IP address)
        (
            [0-9]
            |[1-9][0-9]
            |1[0-9]{2}
            |2[0-4][0-9]
            |25[0-5]
        )                       # match the final number 0 up to 255
        (/(3[012]|[12]\d|\d))?  # optionally match CIDR notation
    )([^\d]|$)                  # the IP must be followed by a non-digit or the EOF to ensure greedy matching
    """, re.VERBOSE)
    
    # TODO: create ipv4_glob_regex and ipv4_range_regex
    
    ip_set = netaddr.IPSet()
    
    for ip in ipv4_cidr_regex.finditer(text):
        #print('IPv4', ip.group(1))
        ip_set.add(ip.group(1))
        
    return ip_set
    
    
def ipv6(text):
    """Returns a netaddr.IPSet"""
    
    # source: http://stackoverflow.com/a/17871737
    # with modifications (moved last line in regex)
    ipv6_regex = re.compile(r"""(
        (
            ([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|          # 1:2:3:4:5:6:7:8
            ([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|         # 1::8             1:2:3:4:5:6::8  1:2:3:4:5:6::8
            ([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|  # 1::7:8           1:2:3:4:5::7:8  1:2:3:4:5::8
            ([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|  # 1::6:7:8         1:2:3:4::6:7:8  1:2:3:4::8
            ([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|  # 1::5:6:7:8       1:2:3::5:6:7:8  1:2:3::8
            ([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|  # 1::4:5:6:7:8     1:2::4:5:6:7:8  1:2::8
            [0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|       # 1::3:4:5:6:7:8   1::3:4:5:6:7:8  1::8  
            :((:[0-9a-fA-F]{1,4}){1,7}|:)|                     # ::2:3:4:5:6:7:8  ::2:3:4:5:6:7:8 ::8       ::     
            fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|     # fe80::7:8%eth0   fe80::7:8%1     (link-local IPv6 addresses with zone index)
            ::(ffff(:0{1,4}){0,1}:){0,1}
            ((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]).){3,3}
            (25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|          # ::255.255.255.255   ::ffff:255.255.255.255  ::ffff:0:255.255.255.255  (IPv4-mapped IPv6 addresses and IPv4-translated addresses)
            ([0-9a-fA-F]{1,4}:){1,4}:
            ((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]).){3,3}
            (25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|          # 2001:db8:3:4::192.0.2.33  64:ff9b::192.0.2.33 (IPv4-Embedded IPv6 Address)
            ([0-9a-fA-F]{1,4}:){1,7}:                          # 1::                              1:2:3:4:5:6:7::
        )
        (/(12[0-8]|1[0-1][0-9]|\d\d|\d))?
    )([^\d]|$)""", re.VERBOSE)
    
    ip_set = netaddr.IPSet()
    
    for ip in ipv6_regex.finditer(text):
        #print('IPv6:', ip.group(1))
        ip_set.add(ip.group(1))
        
    return ip_set
    

def hostnames(text):
    """Returns a set"""

    hostname_regex = re.compile("""(
        (
            (
                [a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9]|
                [a-zA-Z0-9]
            )
            \.
        )+
        (
            [A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9]|
            [A-Za-z0-9]
        )
    )""", re.VERBOSE)

    hostname_set = set()
    
    for hostname in hostname_regex.finditer(text):
        #print('Hostname:', hostname.group(1))
        hostname_set.add(hostname.group(1).lower())
        
    return hostname_set
    
    
def emails(text):
    """Returns a set"""
    
    email_regex = re.compile("""(
        [a-zA-Z][a-zA-Z0-9.-_+']*
        @
        (
            (
                [a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9]|
                [a-zA-Z0-9]
            )
            \.
        )+
        (
            [A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9]|
            [A-Za-z0-9]
        )
    )""", re.VERBOSE)

    email_set = set()

    for email in email_regex.finditer(text):
        email_set.add(email.group(0).lower())
        
    return email_set