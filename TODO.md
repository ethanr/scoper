# TODO

* test Python 3 compatibility
* keep track of hostname and print it with the corresponding IPs
*   possibly create parsers that output a IP->Hostname mapping and takes it as input later (instead of doing a reverse_resolve)
*
* Use a notation that requires less typing
* 
* Add better error handling and detection of incorrectly formatted arguments
* Allow nmap style input of IP addresses (e.g. 192.168.1.1-10)
* Add reverse_resolve operation
* Create proper documentation
* Convert hunt teaming blacklist checking scripts to use this library
* Add detect if argument is a directory and automatically parse all files in that directory

Filters (unary operators): 
* resolve / reverse_resolve
* cidr / expand
* size
* sort

Parsers:
* hostnames
* ipv4
* ipv6
* all - Need a syntax to group together, like a shortcut to parse(1) union parse(2) etc. This would be useful to group directories together. Maybe if the argument is a directory it does the operation to all files within.
* emails

Operations (binary operators):
* union
* intersection
* difference
* equal

* dns_blacklists = resolve(parse_directory(bad_lists)) intersection resolve(parse_directory(record_lists))
* Still need a way to show which file resulted in a hit.  Maybe loop through files and hits after to see which files get hits.
